var;"mat1";mat;0.0;0.9;0.1;50.0;[1.0, 1.0, 1.0]
var;"mat2";mat;0.0;0.0;0.9;50.0;[0.5, 0.5, 0.5]
var;"sph1";sph;[-20.0, -30.0, 100.0];20.0;mat1;8000
var;"sph2";sph;[30.0,30.0, 180.0];50.0;mat2;4500
var;"cam_rebond";cam;3.14;3.14;0.2
var;"atmosphere";atm;0.047;0.12;0.16;100.0
var;"scene";sce;cam_rebond;atmosphere
addObjet;scene;sph1
addObjet;scene;sph2
cam;1.57;1.57;0.005
scene