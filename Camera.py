from Rayon import *
from ConeLumineux import *
import numpy as np
from multiprocessing import Pool
from multiprocessing import Value

class Camera:
    def __init__ (self, ouverture_x , ouverture_y , res_ang):
        """ouverture_x : Ouverture angulaire selon x en radian de la caméra
        ouverture_y : Ouverture angulaire selon y en radian de la caméra
        res_ang : Résolution angulaire en radian de la caméra"""
        self.ouverture_x = ouverture_x
        self.ouverture_y = ouverture_y
        self.res_ang = res_ang
        """Les grandeurs suivante seront calculées après appel à 'set_position_et_direction_focale'"""
        self.lum_i = None
        self.base_focal = None
        self.position = None
        self.direction_focal = None

    def set_position_et_direction_focal(self, position, direction_focal):
        """position : position de la camera
        direction_focale : normale au plan focal (la longueur du vecteur 'direction_focale' ne change rien)"""
        self.position = np.array(position)
        self.direction_focal = np.array(direction_focal)
        """Grandeurs intermédaires"""
        distance_focale = np.sqrt(np.inner(direction_focal, direction_focal))
        self.largeur = distance_focale*self.ouverture_x#Largeur du plan focal visible par la caméra
        self.hauteur = distance_focale*self.ouverture_y#Hauteur du plan focal visible par la caméra
        self.taille_pixel = distance_focale*self.res_ang#côté du carré couvert par un pixel sur le plan focal de la caméra
        """Caractéristiques de l'image"""
        self.image_nrows = int(self.largeur/(self.taille_pixel))#nombre de lignes de l'image de sortie
        self.image_ncols = int(self.hauteur/(self.taille_pixel))#nombre de colonnes de l'image de sortie
        self.base_focal = self.complete_base_orthonormee(self.direction_focal)#Matrice de passage de la base canonique à la base du plan image
        """On remet 'lum_i' à None, car la luminance incidente en cette position et dans cette direction n'est pas la même que celle d'avant le changement de position et de direction"""
        self.lum_i = None

    def init_conteur(self, conteur):
        """Permet d'initialiser un compteur partagé entre les différents processus"""
        global shared_conteur#La variable est global, on fait donc attention à ne l'incrémenter que lors de la première passe
        shared_conteur = conteur

    def get_delta_lum_i(self, scene, nb_rebond, x_img, y_img):
        """Renvoie le cône lumineux associé au pixel (x_img, y_img) de l'image
        pour la scène 'scene' en autorisant 'nb_rebond' rebonds"""
        rayon = self.conversion_coordonnees(x_img, y_img)#On récupère le rayon associé au pixel (x_img, y_img)
        delta_lum = scene.calcul_lum(rayon, nb_rebond)
        cone = ConeLumineux(rayon, 3.14*self.res_ang*self.res_ang, delta_lum)
        return cone

    def thread_get_pixel(self, param):
        """Cette fonction est appelée dans un processus fils, elle appelle elle-même 'get_delta_lum_i'
        mais ne renvoie que la couleur du pixel, et non le cône complet"""
        """Les arguments sont confinés dans un dictionnaire car Pool.map n'accepte de passer qu'un seul argument"""
        scene = param['scene']
        nb_rebond = param['nb_rebond']
        x_img = param['x_img']
        y_img = param['y_img']
        cone = self.get_delta_lum_i(scene, nb_rebond, x_img, y_img)
        """On incrémente le compteur"""
        with shared_conteur.get_lock():
            shared_conteur.value += 1
            if shared_conteur.value%5000 == 0:#On l'incrémente tous les 5000 pixels traités
                pourcentage = shared_conteur.value/(self.image_nrows*self.image_ncols)*100
                print(str(float(int(pourcentage*10))/10)+"%")

        luminance_saturante = 0.26#La luminance saturante est celle du jaune pour un corps noir de température 6000K (une lampe de bureau)
        #On normalise la luminance obtenue par rapport à cette luminance saturante
        luminance_normalisee = 255.*np.array([min(1., (comp/luminance_saturante)) for comp in cone.luminance])#on sature à 'luminance_saturante'
        return luminance_normalisee

    def creer_image(self, scene, nb_rebond):
        """Crée l'image de sortie, en utilisant plusieurs processus fils,
        pour la scène 'scene' en autorisant 'nb_rebond' rebonds"""
        """C'est cette fonction qui est appelée par 'Render'"""
        shared_conteur = Value('i', 0)#Permet aux différents processus d'afficher de temps en temps la progression globale
        p = Pool(processes=None, initializer=self.init_conteur, initargs=(shared_conteur,))#Crée autant de processus qu'il y a de coeur sur la machine (les ressources CPU seront utilisées à 100%)
        input = [{'scene':scene, 'nb_rebond':nb_rebond, 'x_img':x_img, 'y_img':y_img, 'compter': True} for x_img in range (0, self.image_nrows) for y_img in range (0, self.image_ncols)]#On crée la liste des paramètres qui seront passés à 'thread_get_pixel' (un parametre, un appel)
        res = np.array(p.map(self.thread_get_pixel, input))#La fonction 'Pool.map' s'occupe de partitionner 'input' et de distribuer le travail aux différents processus
        self.image = np.reshape(res, (self.image_nrows, self.image_ncols, 3))

    def creer_lum_i(self, scene, nb_rebond):
        """Effectue le même travail que 'creer_image',
        mais n'utilise qu'un seul processus,
        et crée une liste de cônes lumineux au lieu de créer une image"""
        """C'est cette fonction qui est appelée par 'Scene' pour calculer l'effet des rebonds"""
        self.lum_i = []
        for x_img in range(0, self.image_nrows):
            for y_img in range(0, self.image_ncols):
                """Afin de conserver l'indépendance par rapport à la base créée
                par 'complete_base_orthonormee', contrairement à 'creer_image',
                on ne considère que les points qui sont dans le cône défini par l'ouverture de la caméra"""
                if ((x_img - self.image_nrows/2.)**2 + (y_img - self.image_ncols/2.)**2) <= (self.image_nrows/2)**2:
                    cone = self.get_delta_lum_i(scene, nb_rebond, x_img, y_img)
                    if not np.allclose(cone.luminance, np.array([0., 0., 0.]), 1e-5):#Il ne sert à rien d'ajouter un cône si sa luminance est presque nulle
                        self.lum_i.append(cone)

    def conversion_coordonnees(self, x_img, y_img):
        """On convertit les coordonnées (x_img, y_img) sur image en coordonnées dans la base canonique"""
        """x_img et y_img sont les coordonnées sur l'image"""
        x = (self.taille_pixel*x_img - self.largeur/2.)
        y = (self.taille_pixel*y_img - self.hauteur/2.)
        z = 0.
        vecteur = np.array([[x], [y], [z]])#Le vecteur dans la base rattachée au plan focal
        direction = self.base_focal.dot(vecteur) + np.array([[i] for i in self.direction_focal])#On effectue le changement de base
        return Rayon(self.position, direction.transpose()[0])

    def complete_base_orthonormee(self, u):
        """Complète la famille formée du vecteur 'u' en une base orthonormée ('u' doit être non nul)"""
        epsilon = 1e-5
        if np.allclose(u, [0., 0., 0.], atol=epsilon):
            raise Exception("Le vecteur est nul")
        e1 = np.array([1., 0., 0.])
        e2 = np.array([0., 1., 0.])
        e3 = np.array([0., 0., 1.])
        base_canonique = np.array([e1, e2, e3])#La base canonique
        u = u/np.sqrt(np.inner(u, u))#On normalise
        base_complete = [u]
        for j in range (1, 3):#On complete la base
            trouve = False
            i = 0
            while trouve==False:#On cherche le vecteur de la base canonique qui pourrait faire patron (il ne faut pas q'il soit combinaison linéaire des vecteurs déjà construits)
                ei = base_canonique[i]
                projete = np.array([0., 0., 0.])
                for uk in base_complete:#On parcourt tous les éléments déjà construits de la base pour construire le projeté de ei sur Vect(uk)
                    projete = projete + np.inner(uk, ei)*uk
                if np.allclose(ei, projete, atol=epsilon) == False:
                    trouve = True
                else:#Dans ce cas ei est dans le Vect des uk
                    i = i+1
                    if i >= len(base_canonique):#Ceci ne doit mathématiquement jamais se passer
                        raise Exception("Cette exception ne doit jamais se passer")
            ukp1 = ei - projete
            ukp1 = ukp1/np.sqrt(np.inner(ukp1, ukp1))#On normalise
            base_complete.insert(0, ukp1)#On veut que le vecteur fixé au début soit en z
        return np.array(base_complete).transpose()#On n'oublie pas de transposer pour avoir les vecteurs en colonne

    def __str__(self):
        return "Camera"