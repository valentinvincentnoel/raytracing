import numpy as np

class Materiau:
    def __init__(self, a, d, s, alpha, emissivite):
        """a : coefficient de lumière ambiante
        d : coefficient de lumière diffuse
        s : coefficient de lumière spéculaire,
        alpha : coefficient de brillance
        emissivite : émissivité du matériau
        """
        self.s = np.array(s)
        self.d = np.array(d)
        self.a = np.array(a)
        self.alpha = alpha
        self.emissivite = np.array(emissivite)
        self.reflexivite = np.array([1., 1., 1.]) - self.emissivite

    def __str__(self):
        res = "\nMateriau : \n"
        res += "\ts : "+str(self.s)+"\n"
        res += "\td : "+str(self.d)+"\n"
        res += "\ta : "+str(self.a)+"\n"
        res += "\talpha : "+str(self.alpha)+"\n"
        res += "\temissivite : "+str(self.emissivite)
        return res