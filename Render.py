from ConeLumineux import *
from Scene import *
from Materiau import *
from Sphere import *
from Plan import *
from Camera import *
from Atmosphere import *
from Scene import *
from Parser import *
from PIL import Image
import numpy as np
import sys
import json
import time

#paun@ensta.paristech.fr

class Render:
    def __init__(self, nom_fichier, nb_rebond):
        """nom_fichier : nom du fichier de configuration
        nb_rebond : le nombre de rebonds autorisés pour les photons"""
        """On récupère la configuration"""
        fichier = open(nom_fichier, 'r')
        str_expression = fichier.read()
        fichier.close()
        """On demande à un 'Parser' de l'interpréter"""
        parser = Parser()
        parser.parse(str_expression)
        parse_out = parser.interpret()
        """On vérifie que le fichier configuration a spécifié ce qu'il faut"""
        self.verifierOutParse(parse_out)
        """On récupère la scène et la caméra"""
        self.scene = [obj for obj in parse_out if isinstance(obj, Scene)][0]
        print(self.scene)
        self.camera = [obj for obj in parse_out if isinstance(obj, Camera)][0]
        """On place la caméra à l'origine, pointant vers l'axe des z"""
        self.camera.set_position_et_direction_focal([0., 0., 0.], [0., 0., 100.])
        self.nb_rebond = nb_rebond

    def verifierOutParse(self, parse_out):
        """Vérifie que le fichier configuration a au moins spécifié le strict minimum (une seule caméra et une seule scène)"""
        nb_camera = sum(1 for obj in parse_out if isinstance(obj, Camera))#On compte le nombre de caméras
        nb_scene = sum(1 for obj in parse_out if isinstance(obj, Scene))#On compte le nombre de scènes
        if nb_camera != 1:
            raise Exception("Il faut 1 camera : vous en avez spécifiée(s) "+str(nb_camera))
        if nb_scene != 1:
            raise Exception("Il faut 1 scene : vous en avez spécifiée(s) "+str(nb_camera))


    def render(self):
        """Effectue le rendu à proprement parler"""
        t = time.time()#On souhaite calculer le temps qu'a pris le rendu
        print("Début du rendu")
        self.camera.creer_image(self.scene, self.nb_rebond)
        print("Fin du rendu")
        print("Temps pris pour le rendu : "+str(float(int((time.time()-t)*10))/10)+"s")

    def afficher(self):
        """Affiche l'image ('render' doit avoir été appelée avant)"""
        print("Affichage de l'image")
        img = Image.fromarray(np.uint8(self.camera.image), 'RGB')
        img.show()

    def sauver(self, nom_fichier):
        """Enregistre l'image dans 'nom_fichier' ('render' doit avoir été appelée avant)"""
        print("Enregistrement de l'image")
        img = Image.fromarray(np.uint8(self.camera.image), 'RGB')
        img.save(nom_fichier)

    def __str__(self):
        print("Render")









