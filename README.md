# Exécution 
Le fichier à exécuter pour lancer le programme est 'main.py'. Il est aussi
possible d’exécuter 'Tests.py' pour lancer tous les tests unitaires.

# Configuration
Au lancement, le programme propose d’exécuter une des configurations du dossier
'scene'. 
Vous pouvez y ajouter votre propre fichier configuration. L’extension
doit absolument être '.txt'. 
Un fichier de configuration comprend une commande par ligne. Les commandes possibles sont les suivantes :

- *addObjet* qui prend en argument :
   - un objet *s* de type Scene ;
   - un objet *o* de type AbstractObjet.  
   
*addObjet* ajoute l’objet *o* à la scène *s*.

<br/>

- *addLum* qui prend en argument :
   - un objet *s* de type Scene ;
   - un objet *l* de type Lumiere.

*addLum* ajoute la lumière *l* à la scène *s*.

<br/>

- *var* qui prend en argument :
   - une chaine de caractère *nom* ;
   - un objet *o*.
   
*var* déclare la variable de nom *nom* associée à l'objet *o*.

<br/>

- *sph* qui prend en argument :
   - une liste de floats *centre* de taille 3 ;
   - un float *rayon* ;
   - un objet *m* de type Materiau ;
   - un int *temperature*.
 
*sph* crée une sphère de rayon *rayon*, de centre *centre*, fait du matériau *m*, et de température *temperature*.

<br/>

- *mat* qui prend en argument :
   - un float *a* ;
   - un float *d* ;
   - un float *s* ;
   - un float *alpha* ;
   - une liste de floats *emissivite* de taille 3.
  
*mat* renvoie un objet de type Materiau de coefficient de lumière ambiante *a*, de lumière diffuse *d*, de lumière spéculaire *s*, de brillance *alpha* et d’émissivité *emissivite*.

<br/>

- *pl* qui prend en argument :
   - une liste de floats *position* de taille 3 ;
   - une liste de floats *normale* de taille 3 ;
   - un float *e* ;
   - un objet *m* de type Materiau ;
   - un int *temperature*.
  
*pl* renvoie un objet de type Plan de normale *normale*, à la position *position*, d’épaisseur *e*, fait du matériau *m*, et de température *temperature*.

<br/>

- *lum* qui prend en argument :
   - une liste de floats *position* de taille 3 ;
   - une liste de floats *luminance* de taille 3 (rouge, vert, bleu) ;
  
*lum* renvoie un objet de type Lumiere à la position *position* et de luminance *luminance*.

<br/>

- *cam* qui prend en argument :
   - un float *ouverture_x* ;
   - un float *ouverture_y* ;
   - un float *res_ang*.
   
*cam* renvoie un objet de type Camera d’ouverture angulaire selon x *ouverture_x*, d’ouverture angulaire selon y *ouverture_y*, et de résolution angulaire *res_ang*.

<br/>

- *sce* qui prend en argument :
   - un objet *c* de type Camera ;
   - un objet *atm* de type Atmosphere.
  
*sce* renvoie un objet de type Scene dont la camera utilisée pour les rebonds est *c* et dont l’atmosphère est *atm*.

<br/>

- *atm* qui prend en argument :
   - un float *er* ;
   - un float *ev* ;
   - un float *eb* ;
   - un float *ep*.
   
*atm* renvoie un objet de type Atmosphere dont les épaisseurs optiques sont : *er* pour le rouge, *ev* pour le vert, *eb* pour le bleu, et dont l’épaisseur réelle est *ep*.
