import numpy as np

class Atmosphere:

    def __init__(self, e1, e2, e3, epaisseur):
        """e1, e2, e3 sont les épaisseurs optiques respectivement dans le rouge, vert et bleu
        'epaisseur' est l'épaisseur réelle de l'atmosphère"""
        self.epaisseur_optique = np.array([e1, e2, e3])
        self.epaisseur = epaisseur

    def influence(self, lum, depuis, vers):
        """Renvoie la luminance d'un rayon lumineux au point 'vers' en sachant qu'elle était égale à 'lum' au point 'depuis'"""
        lum = np.array(lum)
        depuis = np.array(depuis)
        vers = np.array(vers)
        distance = np.sqrt(np.inner(depuis-vers, depuis-vers))
        """On applique la formule (4) du rapport"""
        lum = np.exp(-(distance*self.epaisseur_optique)/self.epaisseur)*lum#N'est jamais nul, sauf si erreur de float
        lum = np.array([max(0., comp) for comp in lum])#On ramene donc à zero si jamais une erreur d'arrondi à mis une composante négative
        return lum

    def __str__(self):
        res = "\nAtmosphere : \n"
        res += "\tepaisseur_optique : "+str(self.epaisseur_optique)+"\n"
        res += "\tepaisseur : "+str(self.epaisseur)
        return res