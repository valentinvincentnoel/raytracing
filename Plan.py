import numpy as np
from AbstractObjet import *

class Plan(AbstractObjet):

    def __init__ (self, position, att_normale, epaisseur,  materiau, temperature):
        """position : un point du plan
        att_normale : la normale au plan
        epaisseur : l'épaisseur du plan (autour du point 'position')
        (l'épaisseur ne doit pas être trop petite afin d'éviter les erreurs dues aux calculs sur les floats)
        materiau : le matériau du plan
        temperature : la température du plan"""
        AbstractObjet.__init__(self, materiau, temperature)
        self.att_normale = np.array(att_normale)
        self.att_normale = self.att_normale/np.sqrt(np.inner(self.att_normale, self.att_normale))#On normalise
        self.position = np.array(position)
        self.epaisseur = epaisseur

    def normale(self, point):
        """Implémente la classe abstraite de AbstractObjet"""
        epsilon = 1e-3#Permet de gérer la comparaison des floats
        ps = np.inner(point-self.position, self.att_normale)
        if ps >=0:#Le point est au-dessus du plan
            return self.att_normale
        else:#Le point est en-dessous du plan
            return (-1)*self.att_normale

    def intersection(self, rayon):
        """Implémente la classe abstraite de AbstractObjet"""
        ps_dir_norm = np.inner(rayon.direction, self.att_normale)#Le signe de la normale nous est égal
        epsilon = 1e-3#Permet de gérer la comparaison des floats
        if np.isclose(ps_dir_norm,  0, atol=epsilon):
            return []
        l_sol = [np.inner(self.position+self.epaisseur/2 - rayon.depart, self.att_normale)/ps_dir_norm, np.inner(self.position-self.epaisseur/2 - rayon.depart, self.att_normale)/ps_dir_norm]
        """On ne prend que les solutions à l'avant, c'est-à-dire les u positifs"""
        return [sol for sol in l_sol if sol>0]

    def __str__(self):
        return "Plan"