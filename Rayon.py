import numpy as np

class Rayon:
    def __init__(self, depart, direction):
        """depart : le départ du rayon
        direction : la direction dans laquelle part le rayon
        (c'est dans cette direction que seront calculées les intersections)"""
        self.depart = np.array(depart)
        self.direction = np.array(direction)
        self.direction = self.direction/np.sqrt(np.inner(self.direction, self.direction))#On normalise

    def __str__(self):
        return "Rayon"