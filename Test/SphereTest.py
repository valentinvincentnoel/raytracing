import numpy as np
import Sphere
import Materiau
import Rayon

"""Tests unitaires pour la classe 'Sphère'"""
mat = Materiau.Materiau(None, None, None, None, [0., 0., 0.])
sphere = Sphere.Sphere([10., 5., 20.], 2., mat, 1.)

"""On vérifie la normale"""
epsilon = 1e-3
assert(np.array_equal(sphere.normale(np.array([12., 5., 20.])), np.array([1., 0., 0.])))
assert(np.array_equal(sphere.normale(np.array([10., 7., 20.])), np.array([0., 1., 0.])))
assert(np.array_equal(sphere.normale(np.array([10., 5., 22.])), np.array([0., 0., 1.])))
assert(np.allclose(sphere.normale(np.array([11., 6., 20.])), np.array([0.7071, 0.7071, 0.]), epsilon))

"""On vérifie l'intersection"""
epsilon = 1e-5
sphere = Sphere.Sphere([10., 0., 0.], 2., mat, 1.)
rayon = Rayon.Rayon([0., 0., 0.], [1., 0., 0.])
res = sphere.intersection(rayon)
assert(np.allclose(res, [8., 12.], epsilon) or np.allclose(res, [12., 8.], epsilon))

print("Test : Sphere OK")