import numpy as np
import Plan
import Materiau
import Rayon

"""Tests unitaires pour la classe 'Plan'"""
mat = Materiau.Materiau(None, None, None, None, [0., 0., 0.])
plan = Plan.Plan([10., 5., 20.], [1., 8., 11.], 5., mat, 1.)

"""On vérifie la normale"""
normalise = np.array([1., 8., 11.])/np.sqrt(np.inner(np.array([1., 8., 11.]), np.array([1., 8., 11.])))
assert(np.array_equal(plan.normale(np.array([11., 13., 31.])), normalise))
assert(np.array_equal(plan.normale(np.array([9., -3., 9.])), (-1)*normalise))

"""On vérifie l'intersection"""
epsilon = 1e-5
plan = Plan.Plan([1., 1., 1.], [1., 0., 0.], 0.5, mat, 1.)
rayon = Rayon.Rayon([0., 0., 0.], [1., 0., 0.])
res = plan.intersection(rayon)
assert(np.allclose(res, [0.75, 1.25], epsilon) or np.allclose(res, [1.25, 0.75], epsilon))

print("Test : Plan OK")