import numpy as np
import Camera

"""Tests unitaires pour la classe 'Caméra'"""
camera = Camera.Camera(1.57, 1.57, 0.1)
camera.set_position_et_direction_focal([0., 0., 0.], [4., 50., 10])

"""On vérifie que la base focale est bien orthonormée
et que le 3ème vecteur est bien la direction focale"""
base_focale = camera.base_focal
epsilon = 1e-5
assert(np.array_equal(base_focale[:,2], np.array([4., 50., 10.])/np.sqrt(np.inner([4., 50., 10.], [4., 50., 10.]))))
assert(np.isclose(np.inner(base_focale[:,0], base_focale[:,1]),  0, atol=epsilon))
assert(np.isclose(np.inner(base_focale[:,0], base_focale[:,2]),  0, atol=epsilon))
assert(np.isclose(np.inner(base_focale[:,1], base_focale[:,2]),  0, atol=epsilon))
assert(np.isclose(np.inner(base_focale[:,0], base_focale[:,0]),  1.0, atol=epsilon))
assert(np.isclose(np.inner(base_focale[:,1], base_focale[:,1]),  1.0, atol=epsilon))
assert(np.isclose(np.inner(base_focale[:,2], base_focale[:,2]),  1.0, atol=epsilon))

"""On vérifie la conversion de coordonnées"""
camera.set_position_et_direction_focal([0., 0., 0.], [-0.0517, -0.6671, -0.7432])
camera.base_focal = np.array([[0.2673, 0.5345, 0.8018], [-0.9487, 0., 0.3162], [-0.0517, -0.6671, -0.7432]]).transpose()#Base orthonormée choisie à la main
assert(np.isclose(np.inner(base_focale[:,0], base_focale[:,1]),  0, atol=epsilon))
assert(np.isclose(np.inner(base_focale[:,0], base_focale[:,2]),  0, atol=epsilon))
assert(np.isclose(np.inner(base_focale[:,1], base_focale[:,2]),  0, atol=epsilon))
assert(np.isclose(np.inner(base_focale[:,0], base_focale[:,0]),  1.0, atol=epsilon))
assert(np.isclose(np.inner(base_focale[:,1], base_focale[:,1]),  1.0, atol=epsilon))
assert(np.isclose(np.inner(base_focale[:,2], base_focale[:,2]),  1.0, atol=epsilon))
rayon = camera.conversion_coordonnees(2., 3.)
coor = -0.585*camera.base_focal[:,0] + -0.485*camera.base_focal[:,1] + np.array([-0.0517, -0.6671, -0.7432])
coor = coor/np.sqrt(np.inner(coor, coor))
epsilon = 1e-2
assert(np.allclose(rayon.direction, coor, epsilon))

print("Test : Camera OK")
