import numpy as np

class ConeLumineux:
    def __init__ (self, rayon, ouverture, luminance):
        """rayon : le rayon directeur du cône de luminère
        ouverture : l'ouverture en stéradian du cône
        luminance : luminance du rayon directeur"""
        self.rayon = rayon
        self.ouverture = ouverture
        self.luminance = np.array(luminance)

    def __str__(self):
        return "ConeLumineux"


