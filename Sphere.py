import numpy as np
from AbstractObjet import *

class Sphere(AbstractObjet):
    def __init__ (self, centre , rayon, materiau, temperature):
        """centre : centre de la sphère
        rayon : rayon de la sphère
        (Le rayon ne doit pas être trop petit afin d'éviter les erreurs dues aux calculs sur les floats)
        materiau : le matériau de la sphère
        temperature : la température de la sphère"""
        AbstractObjet.__init__(self, materiau, temperature)
        self.centre = np.array(centre)
        self.rayon = rayon

    def normale(self, point):
        """Implémente la classe abstraite de AbstractObjet"""
        xn = (point[0]-self.centre[0])
        yn = (point[1]-self.centre[1])
        zn = (point[2]-self.centre[2])
        vecteur = np.array([xn, yn, zn])
        return vecteur/np.sqrt(np.inner(vecteur,vecteur))#On normalise

    def intersection(self, rayon):
        """Implémente la classe abstraite de AbstractObjet"""
        a = np.inner(rayon.direction, rayon.direction)
        b = 2*np.inner(rayon.direction, rayon.depart-self.centre)
        c = np.inner(rayon.depart-self.centre, rayon.depart-self.centre) - self.rayon*self.rayon
        delta = b*b - 4*a*c
        if delta < 0:
            l_sol = []
        elif delta == 0:
            sol = -b/(2*a)
            l_sol = [sol]
        else:
            sol1 = (-b+np.sqrt(delta))/(2*a)
            sol2 = (-b-np.sqrt(delta))/(2*a)
            l_sol = [sol1, sol2]
        """On ne prend que les solutions à l'avant, c'est-à-dire les u positifs"""
        return [sol for sol in l_sol if sol>0]

    def __str__(self):
        res = "Sphere : \n"
        res += "\tcentre : "+str(self.centre)+"\n"
        res += "\trayon : "+str(self.rayon)+"\n"
        res += "\tabstractobject : "+AbstractObjet.__str__(self)
        return res