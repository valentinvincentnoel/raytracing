import numpy as np
from Sphere import *

class Intersection:
    """Symbolise une unique intersection avec un objet"""
    def __init__ (self, objet, u_inter):
        """objet : l'objet avec qui il y a intersection
        u_inter : la distance entre le point d'intersection et le départ du rayon intersectant l'objet"""
        self.objet = objet
        self.u_inter = u_inter

    def __str__(self):
        return "Intersection avec "+str(self.objet)






