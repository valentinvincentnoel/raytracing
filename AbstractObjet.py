import numpy as np
import math

class AbstractObjet:
    """AbstractObjet possède des méthodes abstraites (intersection, normale)"""
    def __init__(self, materiau, temperature):
        """materiau : le matériau associé à l'objet
        température : la température de l'objet"""
        self.materiau = materiau
        self.temperature = temperature

    def intersection(self, rayon):
        """Renvoie la liste des scalaires 'u' tels que 'u*rayon.direction + rayon.depart' soit un point d'intersection de 'rayon' avec 'self'"""
        """La méthode est abstraite"""
        raise NotImplementedError('subclasses must override intersection()!')

    def normale(self, point):
        """Retourne la normale au point 'point' (ne vérifie pas que le point est bien sur l'objet)"""
        """La méthode est abstraite"""
        raise NotImplementedError('subclasses must override normale()!')

    def luminance_corps_noir(self):
        """Renvoie la luminance d'un corps noir * 10**(-14) (selon la loi de Planck)"""
        """Les différentes grandeurs physiques sont ramenées à des valeurs correctes pour les calculs de floats"""
        """C'est pour cette raison que la luminance renvoyée est la luminance*10**(-14) en unité du système international"""
        long_onde = np.array([6.50, 5.20, 4.80])#[rouge, vert, bleu]
        h = 66.26#constante de planck * 10**(35)
        c = 29.98#célérité de la lumière dans le vide * 10**(-7)
        k = 1.380#constante de boltzmann * 10**(23)
        t = self.temperature*10**(-2)
        if 4.9<t and t<110.:#Si la température est productrice de lumière visible (loi de Wien)
            #Les unités ont été choisies de sorte que l'exposant dans l'exponentiel soit adimensionné et de l'ordre de 1
            luminance = 2*h*c*c/((long_onde**5)*(np.exp(h*c/(long_onde*k*t))-1))#loi de planck
        else:
            luminance = np.array([0., 0., 0.])
        return np.array(luminance)

    def luminance_emise(self, par, vers):
        """Renvoie le vecteur luminance émis par le point 'pars' dans la direction 'vers'"""
        par = np.array(par)
        vers = np.array(vers)
        vers = vers/np.sqrt(np.inner(vers, vers))#On normalise
        normale = self.normale(par)
        normale = normale/np.sqrt(np.inner(normale, normale))#On normalise
        pscal_norm_obs = np.inner(normale, vers)
        """On ramène les produits scalaires à 0 s'ils sont négatifs (empirique)"""
        if(pscal_norm_obs < 0):
            pscal_norm_obs = 0
        return self.materiau.emissivite*pscal_norm_obs*self.luminance_corps_noir()#Voir les formules (1) et (2) du rapport

    def luminance_reflechie(self, lum_i, sur, vers):
        """Renvoie la lumière réfléchie par le point 'sur' dans la direction 'vers' avec pour liste de cônes de lumières incidents 'lum_i'"""
        sur = np.array(sur)
        vers = np.array(vers)
        vers = vers/np.sqrt(np.inner(vers, vers))#On normalise
        normale = np.array(self.normale(sur))
        normale = normale/np.sqrt(np.inner(normale, normale))#On normalise
        lum_r = np.array([0., 0., 0.])
        for cone in lum_i:#On applique la formule (3) du rapport
            depuis = np.array(cone.rayon.direction)
            pscal_lum_norm = np.inner(depuis, normale)
            """On ramène les produits scalaires à 0 s'ils sont négatifs (empirique)"""
            if(pscal_lum_norm < 0):
                pscal_lum_norm = 0
            rm = 2.*pscal_lum_norm*normale - depuis
            pscal_rm_obs = np.inner(rm, vers)
            """On ramène les produits scalaires à 0 s'ils sont négatifs (empirique)"""
            if(pscal_rm_obs < 0):
                pscal_rm_obs = 0
            """Modèle de Phong"""
            partie_ambiente = np.array(self.materiau.a)
            partie_diffusion = np.array(self.materiau.d)*pscal_lum_norm
            partie_reflectance = np.array(self.materiau.s)*math.pow(pscal_rm_obs, self.materiau.alpha)
            lum_r += self.materiau.reflexivite*cone.ouverture*cone.luminance*(partie_diffusion + partie_reflectance + partie_ambiente)
        return lum_r

    def __str__(self):
        res = "\nAbstractObject : \n"
        res += "\ttemperature : "+str(self.temperature)+"\n"
        res += "\tmateriaux : "+str(self.materiau)
        return res