import numpy as np
from Intersection import *
import math
import operator
from ConeLumineux import *

class Lumiere:
    """Symbolise une source lumineuse ponctuelle"""
    def __init__(self, position, luminance):
        """position : la position de la source ponctuelle
        luminance : la luminance de cette source"""
        self.position = np.array(position)
        self.luminance = np.array(luminance)

    def __str__(self):
        return "Lumière"


