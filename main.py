from Render import *
from AbstractObjet import *
from Parser import *
import os

if __name__ == '__main__':
    while True:#Boucle de commande

        """On affiche les scènes disponibles"""
        scene_path = "scenes"
        onlyfiles = [f.split(".")[0] for f in os.listdir(scene_path) if os.path.isfile(os.path.join(scene_path, f))]
        print("Scènes disponibles : ")
        for f in onlyfiles:
            print(f)

        """On demande à l'utilisateur de rentrer un nom de scène"""
        nom_scene = None
        while(nom_scene not in onlyfiles):
            nom_scene = input("Entrez le nom d'une scène parmi la liste ci-dessus: ")
            if nom_scene not in onlyfiles:
                print("Le nom de scene spécifié n'existe pas")

        """On demande à l'utilisateur de rentrer le nombre de rebonds autorisés"""
        nb_rebond = ""
        while((not nb_rebond.isdigit()) or int(nb_rebond) < 0):
            nb_rebond = input("Entrez le nombre de rebonds que devra faire un photon : ")
            if (not nb_rebond.isdigit()) or int(nb_rebond) < 0:
                print("Vous devez spécifier un entier positif")
        """On effectue le rendu"""
        render = Render(os.path.join(scene_path, nom_scene+".txt"), int(nb_rebond))
        render.render()
        print("Rendu terminé")

        """On demande à l'utilisateur de spécifier s'il faut afficher au sauver l'image"""
        str_continu = ""
        while str_continu != "a" and str_continu != "s":
            str_continu = input("Souhaitez vous afficher ou sauver ?(a/s)")
            if str_continu == "a":
                render.afficher()
                break
            elif str_continu == "s":
                nom_fichier = input("Sous quel nom sauvegarder ? ")
                render.sauver(nom_fichier+".png")
                print("Fichier sauvé")
                break
            else:
                print("Le caractère n'est pas reconnu.")
        str_continu = ""

        """On demande à l'utilisateur s'il veut afficher une autre scène"""
        while str_continu != "Y" and str_continu != "N":
            str_continu = input("Souhaitez vous le refaire avec une autre scene ? (Y/N): ")
            if str_continu == "N":
                exit(0)
            elif str_continu == "Y":
                break
            else:
                print("Le caractère n'est pas reconnu.")

