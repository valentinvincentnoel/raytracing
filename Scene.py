from Intersection import *
from Rayon import *
from ConeLumineux import *
import operator
import copy

class Scene:
    def __init__(self, camera_rebond, atmosphere):
        """camera_rebond : la caméra utilisée pour calculer la répartition de la luminance incidente en un point d'intersection
        atmosphere : atmosphère de la scène"""
        self.liste_objet = []
        self.liste_lumiere = []
        self.camera_rebond = camera_rebond
        self.atmosphere = atmosphere

    def add_objet(self, objet):
        self.liste_objet.append(objet)

    def add_lum(self, lum):
        self.liste_lumiere.append(lum)

    def ombre(self, lumiere, point):
        """Renvoie le coefficient à appliquer pour avoir un effet d'ombre au point 'point'
        avec la lumière ponctuelle 'lumière'"""
        rayon = Rayon(lumiere.position, point-lumiere.position)
        """On récupère les intersections entre un rayon lumineux partant de la source ponctuelle
        et allant vers le point 'point', et les objets de la scène"""
        intersections = self.intersections(rayon)
        if intersections != []:
            min_intersection = intersections[0]#On récupère le point d'intersection le plus proche
            u_inter = min_intersection.u_inter
            pt_inter = rayon.depart + u_inter*rayon.direction
            """Si le point d'intersection le plus proche n'est pas le point 'point'
            c'est qu'il y a un objet entre la source et l'objet"""
            if not np.allclose(pt_inter, point, 1e-4):
                return 0.
        return 1.

    def intersections(self, rayon):
        """Renvoie a liste des points d'intersections de 'rayon' avec l'ensemble des objets de la scène"""
        list_intersection = []
        for objet in self.liste_objet:
            list_u_inter = objet.intersection(rayon)
            for u_inter in list_u_inter:
                list_intersection.append(Intersection(objet, u_inter))
        """On trie la liste suivant les 'u' (ie suivant la distance de la source du rayon au point d'intersection)"""
        if list_intersection != []:
            list_intersection.sort(key=operator.attrgetter('u_inter'))
        return list_intersection

    def calcul_lum(self, rayon, nb_rebond):
        """Renvoie la luminance radiative selon le rayon 'rayon',
        en autorisant 'nb_rebonds' rebonds"""
        lum_tot = [0., 0., 0.]
        list_intersection = self.intersections(rayon)#On récupère les intersections avec les objets de la scène
        if list_intersection != []:
            min_intersection = list_intersection[0]#On récupère l'intersection la plus proche
            objet = min_intersection.objet
            u_inter = min_intersection.u_inter
            pt_inter = rayon.depart + u_inter*rayon.direction#Le point d'intersection

            """On calcule la luminance incidente due à d'autres objets, si nous avons encore droit à des rebonds"""
            lum_i = []
            if nb_rebond > 0:
                normale = objet.normale(pt_inter)
                pt_inter = pt_inter + (1e-3)*normale#On s'éloigne un peu de l'objet pour ne pas l'intersecter lors du rebond
                cam = copy.deepcopy(self.camera_rebond)#On copie la caméra car 'camera_rebond' est mutable et est donc partagée entre les différents appels récursifs
                cam.set_position_et_direction_focal(pt_inter, normale)#On place la caméra comme il le faut pour récupérer la lumière incidente
                cam.creer_lum_i(self, nb_rebond-1)
                lum_i = cam.lum_i

            """On calcule la luminance incidente due aux sources ponctuelles"""
            for lumiere in self.liste_lumiere:
                lum = self.ombre(lumiere, pt_inter)*lumiere.luminance
                lum = self.atmosphere.influence(lum, pt_inter, lumiere.position)
                lum_i.append(ConeLumineux(Rayon(pt_inter, lumiere.position-pt_inter), 1., lum))

            lum_r = objet.luminance_reflechie(lum_i, pt_inter, -rayon.direction)#-rayon.direction (signe), car l'observateur est au départ du rayon
            lum_em = objet.luminance_emise(pt_inter, -rayon.direction)
            lum_tot = np.array(lum_em) + np.array(lum_r)
            lum_tot = self.atmosphere.influence(lum_tot, pt_inter, rayon.depart)
        return np.array(lum_tot)

    def __str__(self):
        res = "Scene : \n"
        res += "\tatmosphere : "+str(self.atmosphere)+"\n"
        for obj in self.liste_objet:
            res += str(obj)+"\n"
        return res