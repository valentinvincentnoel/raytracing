import json
from Materiau import *
from Sphere import *
from AbstractObjet import *
from Lumiere import *
from Scene import *
from Camera import *
from Atmosphere import *
from Plan import *
import re



class Expression:
    def interpret(self, liste_expression, variables):
        """Voir le rapport pour une explication de cette méthode"""
        """La méthode est abstraite"""
        raise NotImplementedError('subclasses must override interpret()!')

    def match(self, liste_type, liste_argument):
        """Vérifie que la liste des arguments donnés ('liste_argument') correspond au type d'arguments attendus ('liste_type')"""
        n = len(liste_type)
        m = len(liste_argument)
        if n != m:#On vérifie qu'il y a le bon nombre d'arguments
            raise Exception("Le nombre d'arguments ne correspond pas : "+str(n)+" attendus "+str(m)+" données")
        for i in range(0, n):#On vérifie qu'ils sont du bon type
            if liste_type[i] != "any" and not issubclass(type(liste_argument[i]), liste_type[i]):
                raise Exception("Le type du "+str(i+1)+"ième argument ne correspond pas : "+str(liste_type[i])+" attendu "+str(type(liste_argument[i]))+" donné")

    def recup_argument(self, liste_type, liste_expression, variables):
        """Récupère les arguments définis selon 'liste_type' après avoir vérifié que la 'liste_expression' contient suffisament d'éléments et qu'ils sont du bon type"""
        n = len(liste_type)
        m = len(liste_expression)
        if m < n:#On vérifie qu'il y a encore suffisament d'expressions
            raise Exception("Le nombre d'arguments ne correspond pas : "+str(n)+" attendus "+str(m)+" données")
        liste_argument = []
        for i in range(0, n):#On ajoute les arguments dans le bon sens
            liste_argument.append((liste_expression.pop(0).interpret(liste_expression, variables)))
        self.match(liste_type, liste_argument)#On vérifie qu'ils sont du bon type
        return liste_argument


class Parser:
    def __init__(self):
        """liste_expression : les expressions qu'il faudra interpréter
        (la liste est constituée par 'parse' et interprétée par 'interpret')
        variables : les variables"""
        self.liste_expression = []
        self.variables = {}

    def parse(self, str_liste_expression):
        """Constitue la liste 'liste_expression'"""
        liste_str_expression = re.split("[;\\n\\r]+", str_liste_expression)#On sépare les atomes selon ; ou \n ou \r
        for str_expression in liste_str_expression:
            """On effectue la correspondance entre une commande et la sous classe de 'Expression' associée"""
            if str_expression == "sph":
                self.liste_expression.append(Sph())
            elif str_expression == "mat":
                self.liste_expression.append(Mat())
            elif str_expression == "cam":
                self.liste_expression.append(Cam())
            elif str_expression == "sce":
                self.liste_expression.append(Sce())
            elif str_expression == "atm":
                self.liste_expression.append(Atm())
            elif str_expression == "pl":
                self.liste_expression.append(Pl())
            elif str_expression == "lum":
                self.liste_expression.append(Lum())
            elif str_expression == "addObjet":
                self.liste_expression.append(AddObjet())
            elif str_expression == "addLum":
                self.liste_expression.append(AddLum())
            elif str_expression == "var":
                self.liste_expression.append(Var())
            else:#Si la commande n'est pas connue on ajoute un 'TerminalExpression'
                self.liste_expression.append(TerminalExpression(str_expression))
    def interpret(self):
        """Interprète la liste d'expressions constituées par 'parse'"""
        out = []
        while self.liste_expression != []:
            expression = self.liste_expression.pop(0)
            out.append(expression.interpret(self.liste_expression, self.variables))#On ajoute au flux de sortie
        return out

    def __str__(self):
        res = ""
        for expr in self.liste_expression:
            res = res + "Parser : " + str(expr) + "\n"
        return res

"""Les classes suivantes sont expliquées dans le rapport"""

class TerminalExpression(Expression):
    def __init__(self, str_value):
        self.str_value = str_value

    def interpret(self, liste_expression, variables):
        if self.str_value in variables:
           return variables[self.str_value]
        else:
            try:
                return json.loads(self.str_value)
            except json.JSONDecodeError as e:
                raise Exception("JSON ne parvient pas à interpréter la chaine de caractere "+self.str_value+" : "+e.msg)

    def __str__(self):
        return self.str_value

class Var(Expression):

    def __init__(self):
        pass

    def interpret(self, liste_expression, variables):
        liste_type = [str, "any"]
        liste_argument = self.recup_argument(liste_type, liste_expression, variables)
        nom = liste_argument[0]
        value = liste_argument[1]
        variables[nom] = value

class Sph(Expression):
    def __init__(self):
        pass

    def interpret(self, liste_expression, variables):
        liste_type = [list, float, Materiau, int]
        liste_argument = self.recup_argument(liste_type, liste_expression, variables)
        position = liste_argument[0]
        rayon = liste_argument[1]
        mat = liste_argument[2]
        temperature = liste_argument[3]
        return Sphere(position, rayon, mat, temperature)

class Mat(Expression):
    def __init__(self):
        pass

    def interpret(self, liste_expression, variables):
        liste_type = [float, float, float, float, list]
        liste_argument = self.recup_argument(liste_type, liste_expression, variables)
        a = liste_argument[0]
        d = liste_argument[1]
        s = liste_argument[2]
        alpha = liste_argument[3]
        emissivite = liste_argument[4]
        return Materiau(a, d, s, alpha, emissivite)

class Atm(Expression):
    def __init__(self):
        pass

    def interpret(self, liste_expression, variables):
        liste_type = [float, float, float, float]
        liste_argument = self.recup_argument(liste_type, liste_expression, variables)
        e1 = liste_argument[0]
        e2 = liste_argument[1]
        e3 = liste_argument[2]
        epaisseur = liste_argument[3]
        return Atmosphere(e1, e2, e3, epaisseur)

class Cam(Expression):
    def __init__(self):
        pass

    def interpret(self, liste_expression, variables):
        liste_type = [float, float, float]
        liste_argument = self.recup_argument(liste_type, liste_expression, variables)
        ouverture_x = liste_argument[0]
        ouverture_y = liste_argument[1]
        res_ang = liste_argument[2]
        return Camera(ouverture_x, ouverture_y, res_ang)

class Sce(Expression):
    def __init__(self):
        pass

    def interpret(self, liste_expression, variables):
        liste_type = [Camera, Atmosphere]
        liste_argument = self.recup_argument(liste_type, liste_expression, variables)
        camera = liste_argument[0]
        atmosphere = liste_argument[1]
        return Scene(camera, atmosphere)

class Pl(Expression):
    def __init__(self):
        pass

    def interpret(self, liste_expression, variables):
        liste_type = [list, list, float, Materiau, int]
        liste_argument = self.recup_argument(liste_type, liste_expression, variables)
        position = liste_argument[0]
        att_normale = liste_argument[1]
        epaisseur = liste_argument[2]
        materiau = liste_argument[3]
        temperature = liste_argument[4]
        return Plan(position, att_normale, epaisseur, materiau, temperature)

class Lum(Expression):
    def __init__(self):
        pass

    def interpret(self, liste_expression, variables):
        liste_type = [list, list]
        liste_argument = self.recup_argument(liste_type, liste_expression, variables)
        position = liste_argument[0]
        luminance = liste_argument[1]
        return Lumiere(position, luminance)

class AddObjet(Expression):
    def __init__(self):
        pass

    def interpret(self, liste_expression, variables):
        liste_type = [Scene, AbstractObjet]
        liste_argument = self.recup_argument(liste_type, liste_expression, variables)
        scene = liste_argument[0]
        objet = liste_argument[1]
        scene.add_objet(objet)

class AddLum(Expression):
    def __init__(self):
        pass

    def interpret(self, liste_expression, variables):
        liste_type = [Scene, Lumiere]
        liste_argument = self.recup_argument(liste_type, liste_expression, variables)
        scene = liste_argument[0]
        lum = liste_argument[1]
        scene.add_lum(lum)

